import java.lang.*;
import java.util.*;
import java.awt.*;

public class Interview{

    public static void main(String[] args){
	new Interview().intToRoman(2);
    }

    public String intToRoman(int num) {
        StringBuilder result = new StringBuilder();
        int factor = 0, numLimit = 3, sub, order;
        String subString = "";
        if(num >= 1000) order = 13;
        else if(num >= 900) order = 12;
        else if(num >= 500) order = 11;
        else if(num >= 400) order = 10;
        else if(num >= 100) order = 9;
        else if(num >= 90) order = 8;
        else if(num >= 50) order = 7;
        else if(num >= 40) order = 6;
        else if(num >= 10) order = 5;
        else if(num >= 9) order = 4;
        else if(num >= 5) order = 3;
        else if(num >= 4) order = 2;
        else order = 1;
        while(num > 0){
            if(order == 13){
                sub = 1000; 
                subString = "M";
            }else if(order == 12){
                sub = 900; 
                subString = "CM";
            }else if(order == 11){
                sub = 500; 
                subString = "D";
            }else if(order == 10){
                sub = 400; 
                subString = "CD"; 
            }else if(order == 9){
                sub = 100; 
                subString = "C";
            }else if(order == 8){
                sub = 90; 
                subString = "XC";
            }else if(order == 7){
                sub = 50; 
                subString = "L";
            }else if(order == 6){
                sub = 40; 
                subString = "XL";
            }else if(order == 5){
                sub = 10; 
                subString = "X";
            }else if(order == 4){
                sub = 9; 
                subString = "IX";
            }else if(order == 3){
                sub = 5; 
                subString = "V";
            }else if(order == 2){
                sub = 4; 
                subString = "IV";
            }else{
                sub = 1; 
                subString = "I";
            }
            factor = Math.min(numLimit, num / sub);
	    System.out.println(sub + " " + factor + " " + num);
            while(factor != 0){
                num -= sub * factor;
                result.append(subString);
                factor--;
            }
            order--;
	    System.out.println(result);
        }
        return result.toString();
    }
}
